---
marp: true
title: New C#
description: C# 8.0 - Top 3 features
theme: uncover
paginate: true
_paginate: false
markdown_html: true
---

<style>
@import url('https://fonts.googleapis.com/css?family=Exo+2|PT+Mono');
section {
  font-family: 'Exo 2', 'Ubuntu', 'Segoe UI', sans-serif;
  /* letter-spacing: 1px; */
  letter-spacing: normal;
  word-spacing: .07em;
}
li > code,
p > code {
  font-family: 'PT Mono', 'Hasklig', 'Ubuntu Mono', Consolas, monospace;
  font-size: .9em;
  background-color: transparent;
  color: #0ac;
}
a:not(:hover) {
  color: #4a6 !important;
  text-decoration: underline;
}
pre {
    padding: 1.4em 2em;
}
.hljs-commen t {
    color: #888 !important;
    font-style: italic;
    font-size: .8em;
}

table {
  width: 80%;
}
td, th {
  padding: 2% 3% !important;
}
td:first-child,
th:first-child {
  border-right: 3px solid #666;
}

section.text-animation {
  background: #333;
}
section.text-animation h1 {
  position: relative;
  text-align: center;
  padding: 40px 0;

  /* font-size: calc(6em + 5vw); */
  font-size: 3em;
  white-space: nowrap;

  border: .5vw solid;
  border-radius: 1vw;

  background: linear-gradient(135deg,#40ded7,#ff7676);
  background-size: 300vw 300vw;

  color: #fff;
  /* -webkit-background-clip: text; */
  /* -webkit-text-fill-color: transparent; */
  animation: textAnimate 3s ease-in-out infinite alternate;
}

@keyframes textAnimate {
  from {
    filter: hue-rotate(0deg);
    background-position-x: 0%;
    box-shadow: 0 0 .1em rgba(255,255,255, .8);
  }
  50% {
    box-shadow: 0 0 .2em rgba(255,255,255, .6);
  }
  to {
    filter: hue-rotate(360deg);
    background-position-x: -200vw;
    box-shadow: 0 0 .1em rgba(255,255,255, .8);
  }
}
</style>

<!-- _class: text-animation -->

# New stuff in C#

---

![bg w:100%](images/cp/3kl12.jpg)

---

* C# 7.x recap
* Top 3 new features in C# 8.0
  - `switch` expression
  - `<nullable>` adoption
  - range/index... is it worth it?

---

- [Docs for C# 7.x](https://docs.microsoft.com/en-us/dotnet/csharp/whats-new/csharp-7-3) ~> which features do we actually use?
  - `out var _`
  - local functions
  - confusing? prop lambda ~> `meth?()`

---

![bg left](images/cp/wallhaven-zm232w.jpg)

## Will C# 8.0 provide better equipment?

---

* new `switch` (resembling functional style)
* how to adopt `<nullable>` & does it really matter?
* how to use range/index & is it worth it?

---

- small changes that will make our code cleaner
  - cleaner `using`
  - `??=`

- other things we skip today
  - readonly `struct` members
  - default `interface` members
  - async foreach (aka `IAsyncEnumerable`)

---

## Adoption of C# 8.0

...just use .NET Core 3.x

---

Some of C# 8.0 features are **progression**

...towards **FUNCTIONAL** style

---

<!--
backgroundImage: "linear-gradient(to bottom, #67b8e3, #0288d1)"
_color: #fff
-->

- FP = pure functions
- FP = predicitble code
- FP = less bugs

--

_Is OOP obsolete?_

---

```cs
class Car
{
  int Speed { get; }
}

class BMW : Car
{
  long TrueSpeed { get; }
}
```

---

![bg h:30%](images/cp/failed.png)

---

![bg h:90%](images/cp/input.png)

---

<!-- backgroundImage: none -->
<!-- _color: #f57c00 -->

(common claim)

## <!-- fit --> ...side effects...<br>SUCK

---

<!-- _color: #ec2012 -->

(meaining)

## <!-- fit --> cognitive load<br>SUCKS

---

Functions are easier to test

$$ f(x) \mapsto y $$

<br>

Compose them into app

$$ f(x) = (g \cdot h) (x) $$

---

```cs
public enum Colors {
  Red, Green, Blue
}
```

```cs
int hue;

switch (color) {
  case Colors.Red: hue = 15; break;
  case Colors.Green: hue = 90; break;
  case Colors.Blue: hue = 210; break;
  default: throw new ArgumentException("New color? Fix missing hue");
}
```

---

#### What we used to do

1) Use switch "statement"
2) Add `break`
3) Add `default`
   - even if not needed
4) Add `Unknown`
   - expect `color` as request parameter
5) Format and extract as function
6) Consider static field with mapping
   - `IDictionary<Colors, int> HueFromColor`

---

#### What we can do instead

1) Use switch "expression"
2) Depend on compilator
   - missing cases will break build
    ```xml
    <WarningsAsErrors>CS8509</WarningsAsErrors>
    ```

---

```cs
var hue = color switch
{
  Colors.Red: => 15,
  Colors.Green: => 90,
  Colors.Blue: => 210,

  _: => throw new ArgumentException(
    $"Color {color} was not expected here"
  )
};
```

---

- [Examples](https://devblogs.microsoft.com/dotnet/do-more-with-patterns-in-c-8-0/) with class pattern matching

---

### Adoption of <nullable>

* Consider `!` a code smell
* Avoid `String.Empty` as workaround
* Depends on project type
  - (new) express intent
  - (legacy) reduction of null errors
* Expect adoption cost

---

### Index & Range syntax

- `[X..Y]`, `[^X]`, `[0..^0]` (copy), `[^1]` (last)
  - alternative to `.Skip()` and `.Take()`
  - `?` does it work with ORM (i.e. EF)
- examples
    ```cs
    var array = new[] { 1, 2, 3, 666 };

    var second = array[1];       // = 2
    var last = array[^1];        // = 666

    Index end = ^1;
    Range range = 1..end;
    var subArray = array[range]; // = { 2, 3 }
    // [(inclusive)..(exclusive)]
    ```

---

#### End of topic but...

How about [C# 9.0](https://www.calvinallen.net/csharp-8-is-old-news-onward-to-csharp-9)?

---

## We skipped .NET Core 3.x

- HTTP/2
- Docker memory usage
- Blazor (server-side) [watch this](https://app.pluralsight.com/player?course=c182c990-a56a-4c66-8ec7-4d4496c09897)
  - no webpack 😊
- SingalR auto-reconnect

---

## Knowledge check

```cs
var moreReviewers = switch (string branch, string author) =>
  (_, 'JSek') => new [] { "Darek" },
  ("patch/*", _) => Array.Empty<string>(),
  ("release/*", _) => new [] { "Siekiera", "Michał Cz." };
```

Does it compile?

---

```cs
var parts = "[a].[b].&[x]:[a].[b].&[y]".Split('.');
var min = parts[2..^3];
var max = parts[^1];
```

How to get min/max ('x' and 'y')?

---

### Next steps

- [Quick scan](https://emilcraciun.net/whats-new-in-csharp-7-x-and-8-0-preview/) through C# 7 & 8
- [One](https://app.pluralsight.com/library/courses/devintersection-azureai-session-38) of the best videos from Pluralsight (1h)

---

Now... try it yourself

![h:55%](images/cp/kermit.gif)
