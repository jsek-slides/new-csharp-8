# New C# (8.0) - Features walkthrough

Live on [new-csharp.surge.sh](https://new-csharp.surge.sh)

## Setup

```shell
npm i
```

### Run on localhost

```shell
npm run watch
```

and in another terminal tab

```shell
npm run serve
```

### Run examples

```shell
dotnet --version # Make sure you run at least .NET SDK 3.0.x

cd demo
dotnet run
```

Install additional tool to run *.csx files

```shell
dotnet tool install -g dotnet-script
```

Now you can run standalone snippets

```shell
cd demo
dotnet script text.csx
```

