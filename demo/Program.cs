﻿using System;

namespace demo
{
    public enum Colors
    {
        Red, Green, Blue
    }

    class Program
    {
        private static void Warn(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
            Console.ResetColor();
        }

        static void Main(string[] args)
        {
            Example1();
            Console.WriteLine("----------------");

            Example2();
            Console.WriteLine("----------------");
        }

        #region Old syntax

        static void Example1()
        {
            try
            {
                Console.WriteLine(Accept(Colors.Green));
                Console.WriteLine(Accept(Colors.Blue));
                Console.WriteLine(Accept((Colors)10));
            }
            catch (ArgumentException ex)
            {
                Warn(ex.Message);
            }
        }

        static string Accept(Colors color)
        {
            int hue = 0;

            switch (color)
            {
                case Colors.Red: hue = 15; break;
                case Colors.Green: hue = 90; break;
                case Colors.Blue: hue = 210; break;
                // default: throw new ArgumentException("New color? Fix missing hue");
            }

            return @$"{{ hue: {hue} }}";
        }

        #endregion Old syntax

        #region New syntax

        static void Example2()
        {
            try
            {
                Console.WriteLine(NewAccept(Colors.Green));
                Console.WriteLine(NewAccept(Colors.Blue));
                Console.WriteLine(NewAccept((Colors)10));
                // BTW, enums are not checked: https://stackoverflow.com/a/618312
            }
            catch (ArgumentException ex)
            {
                Warn(ex.Message);
            }
        }

        static string NewAccept(Colors color)
        {
            var hue = color switch
            {
                Colors.Red => 15,
                Colors.Green => 90,
                Colors.Blue => 210,
                _ => throw new ArgumentException($"New color {color:G}? Fix missing hue"),
            };

            return @$"{{ hue: {hue} }}";
        }
    }

    #endregion New syntax
}
