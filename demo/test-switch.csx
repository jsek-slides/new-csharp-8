static void Main()
{
    string branch = "feature/*", author = "Michał";

    var moreReviewers = switch (string branch, string author) =>
        (_, "JSek") => new [] { "Darek" },
        ("patch/*", _) => Array.Empty<string>(),
        ("release/*", _) => new [] { "Siekiera", "Michał Cz." };

    Console.WriteLine(string.Join(", ", moreReviewers));
}

#region solution
static void Main2()
{
    string branch = "feature/*", author = "Michał";
    var moreReviewers = (branch, author) switch
    {
        (_, "JSek") => new [] { "Darek" },
        ("patch/*", _) => Array.Empty<string>(),
        ("release/*", _) => new [] { "Siekiera", "Michał Cz." },
        _ => new [] { "JSek" },
    };
    Console.WriteLine(string.Join(", ", moreReviewers));
}
#endregion solution

Main();