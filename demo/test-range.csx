using System.Text.RegularExpressions;

static void Main()
{
    ParseRange("[a].[b].&[x]:[a].[b].&[y]");
    ParseRange("[Time].[Year].&[2022]:[Time].[Year].&[2028]");
    ParseRange("[Time].[Year].&[2022]:null");
    ParseRange("[Time].[Year].&[2022]");
    ParseRange("null:[Time].[Year].&[2022]");
}

static void ParseRange(string uniqueName)
{
    var parts = uniqueName.Split(new [] { '.', ':'});
    var min = parts[2];
    var max = parts[^1];

    Console.WriteLine(string.Join(" : ", new [] { min, max }));
}

#region solution

static void ParseRange2(string uniqueName)
{
    var cleanUniqueName = Regex.Replace(uniqueName, @"(:?null:?)|\]|&|\[", "");
    var parts = Regex.Split(cleanUniqueName, @"\.|\:");

    var min = parts[2];
    var max = parts[^1]; // last

    Console.WriteLine(string.Join(" : ", new [] { min, max }));
}

#endregion solution

Main();